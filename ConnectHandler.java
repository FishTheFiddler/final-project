package Final360;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

public class ConnectHandler extends BaseHandler {

    public void handleRequest(HttpExchange httpExchange, String playerName)  throws IOException
    {
        // Perform data validation.
        String errorMessage = "";
        // Check to make sure that the first character is a letter.
        if (!isLetter(playerName.charAt(0))) {
            System.out.println("print 1");
            errorMessage = "The first letter of the name must be a letter.";
        }
        // Check that all the characters of the string are alpha numeric only.
        for (int i = 0; i < playerName.length(); i++) {
            if (!isLetter(playerName.charAt(i)) && !Character.isDigit(playerName.charAt(i))) {
                errorMessage = "Name can only have alpha numeric characters.";
                System.out.println("print 2");
            }
        }
        // Chack to make sure that the name is not already used.
        if (Roster.getInstance().isNameTaken(playerName)) {
            errorMessage = "The name, " + playerName + ", is already taken.";
            System.out.println("print 3");
        }
        String opponent = "";
        if (errorMessage == "") {
            opponent = Roster.getInstance().addPlayer(playerName);
        }
        handleOpponentNameErrorResponse(httpExchange, opponent, errorMessage);
    }

    private boolean isLetter(char letter) {
        System.out.println((int)letter);
        System.out.println((int)(new String("a").charAt(0)));
        System.out.println("");
        if (
                ((int)letter >= (int)(new String("a").charAt(0)) && (int)letter <= (int)(new String("z").charAt(0))) ||
                        ((int)letter >= (int)(new String("A").charAt(0)) && (int)letter <= (int)(new String("Z").charAt(0)))
        ) {
            return true;
        } else {
            return false;
        }
    }

}

