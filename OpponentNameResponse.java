package Final360;

public class OpponentNameResponse {

    public String opponent;
    public String error_message;


    public String getOpponent(){
        return opponent;
    }
    public String setOpponent(String jname){
        this.opponent = jname;
        return opponent;
    }
    public String getErrorMessage(){
        return error_message;
    }
    public String setErrorMessage(String jname){
        this.error_message = jname;
        return error_message;
    }

    public String toString(){
        return "t";
    }

}
