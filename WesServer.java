package Final360;

import MassEffectJSON.CharacterHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class WesServer {

    public static void main(String[] args) {

        boolean running = false;
        try {
            HttpServer server = HttpServer.create(new InetSocketAddress("localhost", 8000), 0);
            server.createContext("/", new InterfaceHandler());
            server.createContext("/connect", new ConnectHandler());
            server.createContext("/get_opponent_name", new OpponentHandler());
            server.createContext("/get_word", new GetWordHandler());
            server.createContext("/recognized_winner", new RegisteredWinnerHandler());
            server.createContext("/get_stats", new StatsHandler());
            ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
            server.setExecutor(threadPoolExecutor);
            server.start();
            System.out.println("Server started on port 8000");
            running = true;

            while (running) {
                System.out.println("Server is awaiting request.  ----  " + System.currentTimeMillis());
                Thread.sleep(5000);

            }
        } catch (IOException | InterruptedException e) {
            System.out.println("Error Connecting.");
        }
    }
}
