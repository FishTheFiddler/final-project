package Final360;

import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

public class GetWordHandler extends BaseHandler {

    public void handleRequest(HttpExchange httpExchange, String playerName)  throws IOException
    {
        Map<String,String> params = BaseHandler.queryToMap(httpExchange.getRequestURI().getQuery());
        String guessed = params.get("guessed");
        if (guessed.contains("true") && Roster.getInstance().getTeam(playerName).winner == "") {
            System.out.println(guessed);
            Roster.getInstance().getTeam(playerName).winner = playerName;
            int i = Roster.getInstance().getPlayerIndex(playerName);
            Roster.getInstance().getTeam(playerName).wins.set(i, Roster.getInstance().getTeam(playerName).wins.get(i) + 1);
            i = 1 - i;
            Roster.getInstance().getTeam(playerName).loses.set(i, Roster.getInstance().getTeam(playerName).loses.get(i) + 1);
        }
        Roster.getInstance().update(playerName);
        handleGetWordResponse(
                httpExchange,
                Roster.getInstance().getScrambledWord(playerName),
                Roster.getInstance().getAnswer(playerName),
                Roster.getInstance().getWinner(playerName),
                Roster.getInstance().getRemainingTime(playerName));
    }

}

