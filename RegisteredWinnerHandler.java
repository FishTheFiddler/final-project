package Final360;

import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;
import java.io.OutputStream;

public class RegisteredWinnerHandler extends BaseHandler {

    public void handleRequest(HttpExchange httpExchange, String playerName)  throws IOException
    {
        String opponentName = Roster.getInstance().getOpponent(playerName);
        if (Roster.getInstance().winnerWasRecognized.get(opponentName)) {
            Roster.getInstance().winnerWasRecognized.put(playerName, false);
            Roster.getInstance().winnerWasRecognized.put(opponentName, false);
            Roster.getInstance().getTeam(playerName).winner = "";
            Roster.getInstance().getTeam(playerName).startTime = 0;
            Roster.getInstance().update(playerName);
        } else {
            Roster.getInstance().winnerWasRecognized.put(playerName, true);
        }

        OutputStream outputStream = httpExchange.getResponseBody();
        String jsonResponse = "{\"placeholder\": \"nothing\"}";
        httpExchange.getResponseHeaders().set("Content-Type", "text/json");
        httpExchange.sendResponseHeaders(200, jsonResponse.length());
        outputStream.write(jsonResponse.getBytes());
        outputStream.flush();
        outputStream.close();
    }

}

